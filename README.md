# Password Checker (FaaS)

## Tugas Kelompok: Activity Tracker

Komputasi Awan 2022/2023-1

Anggota Kelompok:

- Ghifari Aulia Azhar Riza - 1906306773
- Michael Felix Haryono - 1906398326
- Muhamad Andre Gunawan - 1906400021
- Rico Tadjudin - 1906398364
- Zaki Indra Yudhistira - 1906350906

## Deskripsi

Berikut adalah kode satu fungsi password_checker yang akan mengecek validasi password yang diterima sebagai input request. Fungsi password_checker telah di-deploy pada servis GCP Cloud Functions (Function-as-a-Service). 

## Spesifikasi Pengecekan Password

Pertama, fungsi akan mengecek apakah password valid sesuai aturan. Aturan yang ada adalah password minimal terdiri dari 8 karakter dan minimal memiliki 1 huruf kapital, 1 huruf kecil, dan 1 angka. 

Selain itu, fungsi juga akan mengecek apakah password terlalu umum dengan menyamakan password kepada sebuah database (dalam kode repo ini static list saja).

Jika password belum menemui kedua kondisi di atas, maka fungsi akan membalikkan boolean False dan pesan mengapa masih belum valid. Jika sudah valid maka fungsi akan membalikkan boolean True.

## Input dan Output

Input : String

Output : {
	"is_valid" : Bool,
	"message" : String,
}