import re
import os

def password_checker(request):
    """Check password in body of POST request
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response boolean True or False whether the password is valid or not 
        The response is sent as a Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    # Set CORS headers for the preflight request
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': os.environ.get('ALLOW_ORIGIN_URL', '*'),
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': os.environ.get('ALLOW_ORIGIN_URL', '*')
    }


    # --- FaaS Code ---
    # Get Request data input
    request_json = request.get_json()
    if not request_json or not 'password' in request_json:
        # If request is not json and 'password' is not present as key, return False
        return ({
            "is_valid" : False,
            "message" : "Bad Request",
        }, 400, headers)


    # Check Password Validity
    password = request_json["password"].replace("-", "")
    if not re.fullmatch(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$', password):
        return ({
            "is_valid" : False,
            "message" : "Password harus terdiri dari minimal 8 karakter dan memiliki minimal 1 huruf kapital, 1 huruf kecil, dan 1 angka.",
        }, 200, headers)


    # Check if password is too common
    common_passwords = ["abc123456", "password123", "pass123456", "pass0000", "p4ssw0rd"] # Contoh list password common
    if request_json["password"].lower() in common_passwords:
        return ({
            "is_valid" : False,
            "message" : "Password terlalu umum dan mudah ditebak!",
        }, 200, headers)
        
        
    # Password valid
    return ({
        "is_valid" : True,
        "message" : "",
    }, 200, headers)